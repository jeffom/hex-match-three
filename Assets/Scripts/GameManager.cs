﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] BasicMenuViewController viewController;
    [SerializeField] Grid grid;
    [SerializeField] GameConfig config;
    [SerializeField] InputHandler inputHandler;
#pragma warning restore

    int currentScore = 0;
    int comboCount = 0;
    int movesLeft;
    float comboTime;
    
    private void Start()
    {
        ResetGame();
        grid.OnMatchDone += OnHexagonsExploded;
        viewController.OnRetryPressed += ResetGame;
    }

    private void OnDestroy()
    {
        grid.OnMatchDone -= OnHexagonsExploded;
        viewController.OnRetryPressed -= ResetGame;
    }

    private void Update()
    {
        if(movesLeft <= 0)
        {
            //game over
            inputHandler.enabled = false;
            GameOver();
        }

        if(comboCount > 0)
        {
            comboTime += Time.deltaTime;

            viewController.SetProgressCombo(1-comboTime / config.ComboTimer);

            if(comboTime >= config.ComboTimer)
            {                
                comboCount = 0;
                viewController.SetProgressCombo(0);
            }
        }
    }

    void GameOver()
    {
        comboCount = 0;
        viewController.SetRetryButtonVisible(true);
        viewController.ShowGameOver();
        viewController.SetProgressCombo(0f);
    }
       
    
    private void OnHexagonsExploded(int matches)
    {
        comboCount++;
        comboTime = 0;

        var score = (int)(config.BaseScore * matches * config.MatchMult * comboCount);       
        currentScore += score;    
        viewController.UpdateScore(currentScore);
        viewController.UpdateMovesLeft(--movesLeft);                
        viewController.UpdateComboCount(comboCount);

        if (matches <= 3) viewController.UpdateGameInfo("Good");
        else if (matches == 4) viewController.UpdateGameInfo("Great");
        else if (matches > 4) viewController.UpdateGameInfo("Amazing");

        CancelInvoke();
        Invoke("HideGameInfo", 3);
    }

    void HideGameInfo()
    {
        viewController.UpdateGameInfo(string.Empty);
    }

    void ResetGame()
    {
        movesLeft = config.MaxMoves;
        currentScore = 0;
        comboCount = 0;
        comboTime = 0;
        viewController.UpdateMovesLeft(config.MaxMoves);
        viewController.UpdateScore(0);

        inputHandler.enabled = true;
        viewController.SetRetryButtonVisible(false);
        viewController.DismissGameOver();
        viewController.SetProgressCombo(0);
        viewController.UpdateGameInfo(string.Empty);
    }
}
