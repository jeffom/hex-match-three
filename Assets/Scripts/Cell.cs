﻿using UnityEngine;

public class Cell : MonoBehaviour
{
    public Vector3 startMovePos;
    public Vector3 endMovePos;
    public Vector2 coords;        
    public bool moveDown;
    public bool isMarked;
    public SpriteRenderer fill;
    public Color Color {
        get { return fill.color; }
        set { fill.color = value; }
    }
    
    public void SetFill(GameObject fill)
    {
        this.fill = fill.gameObject.GetComponent<SpriteRenderer>();
    }    
}
