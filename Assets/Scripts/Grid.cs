﻿using System;
using UnityEngine;

public class Grid : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] float outerRadius;
    [SerializeField] GameObject hexPrefab;
    [SerializeField] GameObject hexFillPrefab;
    [SerializeField] int width; //grid columns
    [SerializeField] int height; //grid rows
    public bool IsGridUpdating => isGridDirty;
#pragma warning restore

    GameConfig config;
    Cell[,] hexagons;
    Transform cellsParent;
    Vector2 gridCoords;
    float sideLength;
    float sqrtThree;
    bool isGridDirty;
    float currentDropTime = 0f;

    public Action<int> OnMatchDone;

    internal Cell FindCell(Vector2 mousePos)
    {
        var currentPos = transform.position;
        mousePos.x -= currentPos.x;
        mousePos.y -= currentPos.y;

        var axialPoint = ScreenToAxial(mousePos);
        var offSet = AxialToOffset(axialPoint);
        return hexagons[(int)offSet.x, (int)offSet.y];
    }

    public bool IsPosWithiGrid(Vector2 mousePos)
    {
        var currentPos = transform.position;
        mousePos.x -= currentPos.x;
        mousePos.y -= currentPos.y;

        var axialPoint = ScreenToAxial(mousePos);
        var offSet = AxialToOffset(axialPoint);
        return IsWithingGrid(offSet);
    }

    private void Awake()
    {
        sideLength = outerRadius / 2;
        sqrtThree = Mathf.Sqrt(3);

        config = FindObjectOfType<GameConfig>();
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetHexagons();
        InitGrid();
    }

    private void Update()
    {
        int moveCount = 0;
        if (isGridDirty)
        {
            //need to move some objects
            currentDropTime += Time.deltaTime;
            //check grid
            for (int i = 0; i < hexagons.GetLength(0); i++)
            {
                for (int j = 0; j < hexagons.GetLength(1); j++)
                {
                    var cell = hexagons[i, j];
                    if (cell.moveDown)
                    {
                        moveCount++;
                        var source = cell.startMovePos;
                        var dest = cell.transform.position;

                        cell.fill.transform.position = Vector3.Lerp(source, dest, currentDropTime / config.DropTime);
                        var dist = (dest - cell.fill.transform.position).magnitude;
                        if (Mathf.Abs(dist) < .1f)
                        {
                            cell.fill.transform.position = dest;
                            cell.moveDown = false;
                        }
                    }
                }
            }

            if (moveCount == 0)
            {
                currentDropTime = 0f;
                isGridDirty = false;
            }
        }
    }

    /// <summary>
    /// Build grid starting from bottom,
    /// bottom is left is (0,0)
    /// </summary>    
    void InitGrid()
    {
        hexagons = new Cell[height, width];
        Vector2 axial = new Vector2();
        Vector2 screen = new Vector2();
        var availableColors = config.CellColors;

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                var newHex = Instantiate(hexPrefab);
                var hexFill = Instantiate(hexFillPrefab);

                var cell = newHex.AddComponent<Cell>();
                cell.SetFill(hexFill);
                axial.x = i;
                axial.y = j;
                axial = OffSetToAxial(axial);
                screen = AxialToScreen(axial);
                screen += (Vector2)transform.position; //to follow grid object

                hexagons[i, j] = cell;
                cell.coords = axial;

                var cellColor = availableColors[UnityEngine.Random.Range(0, availableColors.Length)];
                hexFill.transform.SetParent(cellsParent);
                hexFill.transform.position = screen;
                cell.Color = cellColor;

                newHex.transform.SetParent(cellsParent);
                newHex.transform.localPosition = screen;
            }
        }
    }

    void ResetHexagons()
    {
        var newGo = new GameObject("Hexagons");
        newGo.transform.SetParent(transform);

        cellsParent = newGo.transform;
    }

    /// <summary>
    /// Just offset the fills and animate them later
    /// </summary>
    /// <param name="row"></param>
    /// <param name="column"></param>
    public void DropHexagons(int totalMaches)
    {
        for (int i = 0; i < hexagons.GetLength(0); i++)
        {
            for (int j = 0; j < hexagons.GetLength(1); j++)
            {
                var cell = hexagons[i, j];
                if (cell.isMarked)
                {
                    bool hasMarkedColumn = false;
                    //check cells above
                    for (int idxTop = i; idxTop < hexagons.GetLength(0); idxTop++)
                    {
                        var topCell = hexagons[idxTop, j];
                        if (!topCell.isMarked)
                        {
                            //move color to this cell
                            cell.Color = topCell.Color;
                            cell.moveDown = true;
                            cell.startMovePos = topCell.transform.position;
                            cell.fill.transform.position = topCell.transform.position;
                            hasMarkedColumn = true;
                            topCell.isMarked = true;
                            break;
                        }
                    }

                    //all above are marked
                    if (!hasMarkedColumn)
                    {
                        var topMostCell = hexagons[height - 1, j];
                        var cellColor = config.CellColors[UnityEngine.Random.Range(0, config.CellColors.Length)];

                        cell.fill.transform.position = topMostCell.transform.position + Vector3.up * outerRadius * 0.866f; //approx. of inner radius
                        cell.startMovePos = cell.fill.transform.position;
                        cell.moveDown = true;
                        cell.Color = cellColor;
                    }
                }
            }
        }
        isGridDirty = true;

        OnMatchDone?.Invoke(totalMaches);
    }

    //Check if the coordinates are withing the currentGrid
    public bool IsWithingGrid(Vector2 offSetIndexes)
    {
        return !(offSetIndexes.x < 0 || offSetIndexes.y < 0 || offSetIndexes.x >= width || offSetIndexes.y >= height);
    }

    //Reset grid cells
    public void ResetCells()
    {
        for (int i = 0; i < hexagons.GetLength(0); i++)
        {
            for (int j = 0; j < hexagons.GetLength(1); j++)
            {
                var cell = hexagons[i, j];
                cell.isMarked = false;
            }
        }
    }

    //Check if the cells are neighbours
    public bool AreCellsNeighbourds(Cell a, Cell b)
    {
        //axial grid sitance calculation
        var dist = (Mathf.Abs(a.coords.x - b.coords.x) + Mathf.Abs(a.coords.x + a.coords.y - b.coords.x - b.coords.y) + Math.Abs(a.coords.y - b.coords.y)) / 2;
        return (int)dist == 1;
    }

    public bool AreCellsTheSameColor(Cell a, Cell b)
    {
        //ignoring alpha because we're using it for highlight
        return ((a.Color.r == b.Color.r) && (a.Color.g == b.Color.g) && (a.Color.b == b.Color.b));
    }

    /// <summary>
    /// convertes offset to axial
    /// </summary>
    /// <param name="screenPoin"></param>
    /// <returns></returns>
    public static Vector2 OffSetToAxial(Vector2 p)
    {
        //inverting axis because we're rotating the hexagon
        p.x = (p.x - (Mathf.Floor(p.y / 2)));
        return p;
    }

    /// <summary>
    /// Converting axial coords to offset
    /// </summary>
    /// <param name="screenPoin"></param>
    /// <returns></returns>
    public static Vector2 AxialToOffset(Vector2 axialPt)
    {
        //inverting axis because we're rotating the hexagon
        axialPt.x = (axialPt.x + (Mathf.Floor(axialPt.y / 2)));
        return axialPt;
    }

    /// <summary>
    /// Getting screen pixel coords from axial coords
    /// </summary>
    /// <param name="screenPoin"></param>
    /// <returns></returns>
    Vector2 ScreenToAxial(Vector2 screenPoint)
    {
        //inverting tiles because we're rotating the hexagon
        var axialPoint = new Vector2();
        //gettin value for x axis
        axialPoint.x = (screenPoint.y - (screenPoint.x / sqrtThree)) / (sqrtThree * sideLength);
        //getting value for y
        axialPoint.y = screenPoint.x / (1.5f * sideLength);
        //getting implicit z value
        var cubicZ = CalculateCubicZ(axialPoint);

        var x = Mathf.Round(axialPoint.x);
        var y = Mathf.Round(axialPoint.y);
        var z = Mathf.Round(cubicZ);
        if (x + y + z == 0)
        {
            screenPoint.x = x;
            screenPoint.y = y;
        }
        else
        {
            var dx = Mathf.Abs(axialPoint.x - x);
            var dy = Mathf.Abs(axialPoint.y - y);
            var dz = Mathf.Abs(cubicZ - z);
            if (dx > dy && dx > dz)
            {
                screenPoint.x = -y - z;
                screenPoint.y = y;
            }
            else if (dy > dx && dy > dz)
            {
                screenPoint.x = x;
                screenPoint.y = -x - z;
            }
            else if (dz > dx && dz > dy)
            {
                screenPoint.x = x;
                screenPoint.y = y;
            }
        }

        return screenPoint;
    }

    Vector2 AxialToScreen(Vector2 axialPoint)
    {
        //inverting tiles because we're rotating the hexagon
        var tileY = sqrtThree * sideLength * (axialPoint.x + (axialPoint.y / 2));
        var tileX = 3 * sideLength / 2 * axialPoint.y;
        axialPoint.x = tileX;
        axialPoint.y = tileY;
        return axialPoint;
    }

    float CalculateCubicZ(Vector2 axialPoint)
    {
        return -axialPoint.x - axialPoint.y;
    }
}
