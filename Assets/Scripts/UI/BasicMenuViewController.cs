﻿using UnityEngine;
using System;

public class BasicMenuViewController : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] BasicMenuView view;
#pragma warning restore

    public Action OnRetryPressed;

    private void Awake()
    {
        view.SetMovesLeft(0);
        view.SetScore(0);
    }
       
    public void UpdateMovesLeft(int movesLeft)
    {
        view.SetMovesLeft(movesLeft);
    }

    public void UpdateScore(int newScore)
    {
        view.SetScore(newScore);
    }

    public void SetRetryButtonVisible(bool visible)
    {
        view.SetButtonEnabled(visible); 
    }

    public void ShowGameOver()
    {
        view.SetGameOverVisible(true);
    }

    public void DismissGameOver()
    {
        view.SetGameOverVisible(false);
    }

    public void RetryClicked()
    {
        OnRetryPressed?.Invoke();
    }

    public void SetProgressCombo(float val)
    {
        view.SetProgressVisible(val > 0);
        view.SetProgressValue(val);
    }

    public void UpdateGameInfo(string info)
    {
        view.SetGameInfo(info);
    }

    public void UpdateComboCount(int combo)
    {
        view.SetComboValue(combo);
    }
}
