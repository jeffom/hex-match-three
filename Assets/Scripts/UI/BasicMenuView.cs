﻿using System.Security;
using UnityEngine;
using UnityEngine.UI;

public class BasicMenuView : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] Text labelScore;
    [SerializeField] Text labelMovesLeft;
    [SerializeField] Text labelGameInfo;
    [SerializeField] Text labelComboInfo;
    [SerializeField] Text labelComboValue;
    [SerializeField] Button retryButton;
    [SerializeField] GameObject gameOverOverlay;
    [SerializeField] Image progress;
#pragma warning restore

    public void SetScore(int score)
    {
        labelScore.text = score.ToString();
    }

    public void SetMovesLeft(int movesLeft)
    {
        labelMovesLeft.text = movesLeft.ToString();
    }

    public void SetButtonEnabled(bool visible)
    {
        retryButton.gameObject.SetActive(visible);
    }

    public void SetGameOverVisible(bool visible)
    {
        gameOverOverlay.gameObject.SetActive(visible);
    }

    public void SetProgressValue(float val)
    {
        progress.fillAmount = val;
    }

    public void SetProgressVisible(bool visible)
    {
        progress.gameObject.SetActive(visible);
        labelComboInfo.gameObject.SetActive(visible);
        labelComboValue.gameObject.SetActive(visible);
    }

    public void SetGameInfo(string info)
    {
        labelGameInfo.text = info;
    }

    public void SetComboValue(int combo)
    {
        labelComboValue.text = "X" + combo.ToString();
    }
}
