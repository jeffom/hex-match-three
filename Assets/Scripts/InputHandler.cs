﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class InputHandler : MonoBehaviour
{
#pragma warning disable 649
    [SerializeField] Camera gameCamera;
    [SerializeField] LineRenderer lineRenderer;
#pragma warning restore

    Grid grid;
    Cell startCell;
    Cell prevCell;
    List<Cell> matches;

    private void Awake()
    {
        grid = FindObjectOfType<Grid>();
        matches = new List<Cell>();
        lineRenderer.positionCount = 0;
    }

    private void Update()
    {
        if (grid.IsGridUpdating) return; //wait until the grid has finished updating to handle input

        var mousePos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {            
            if (!grid.IsPosWithiGrid(mousePos)) return;

            var cell = GetCurrentMousePosCell();
            startCell = cell;
            var newColor = cell.fill.color;
            newColor.a = 0.5f;

            cell.Color = (newColor);
            startCell = cell;
            prevCell = startCell;
            cell.isMarked = true;
            matches.Add(cell);
            lineRenderer.positionCount = matches.Count;
            lineRenderer.SetPosition(matches.Count - 1, cell.transform.position);
        }

        if (Input.GetMouseButton(0))
        {
            if (grid.IsPosWithiGrid(mousePos) && matches.Count > 0)
            {
                CheckSelection();                
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            //do the match three stuff  
            if (matches.Count >= 3)
            {
                grid.DropHexagons(matches.Count);
            }

            ResetSelection();
        }
    }

    void CheckSelection()
    {
        //detect the line
        var cell = GetCurrentMousePosCell();
        if (cell != prevCell && !cell.isMarked)
        {
            if (grid.AreCellsNeighbourds(cell, prevCell) && grid.AreCellsTheSameColor(cell, prevCell))
            {
                matches.Add(cell);
                prevCell = cell;
                cell.isMarked = true;
                var newColor = cell.Color;
                newColor.a = 0.5f;
                cell.Color = (newColor);
                lineRenderer.positionCount = matches.Count;
                lineRenderer.SetPosition(matches.Count - 1, cell.transform.position);
            }
        }
        if(cell.isMarked)
        {
            if (matches.Count > 1)  //do we have a line drawed?
            {
                //handle backtracking the selection                
                if (cell == matches[matches.Count - 2]) //did go back?
                {
                    //backtrack the selection
                    lineRenderer.positionCount = matches.Count - 1;                    
                    prevCell.isMarked = false;
                    var newColor = prevCell.Color;
                    newColor.a = 1f;
                    prevCell.Color = newColor;
                    matches.Remove(prevCell);
                    prevCell = cell;
                }
            }
        }
    }

    void ResetSelection()
    {
        foreach (var c in matches)
        {
            var newColor = c.Color;
            newColor.a = 1f;
            c.Color = newColor;
        }

        grid.ResetCells();
        matches.Clear();
        lineRenderer.positionCount = 0;
    }      

    Cell GetCurrentMousePosCell()
    {
        var pos = Input.mousePosition;
        pos = gameCamera.ScreenToWorldPoint(pos);
        return grid.FindCell(pos);
    }
}
