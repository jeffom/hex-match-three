﻿using UnityEngine;

//todo turn this into a scriptable object if we have the time
public class GameConfig : MonoBehaviour
{
#pragma warning disable 649 
    [SerializeField] Color[] availableColors;
    [SerializeField] int maxMoves;
    [SerializeField] float dropTime = 1f;
    [SerializeField] int baseBlockPoint = 100;
    [SerializeField] float blockMultiplier = 1.5f; //multiplier for each new block    
    [SerializeField] float comboTimer = 3f;
#pragma warning restore

    public Color[] CellColors => availableColors;
    public int MaxMoves => maxMoves;
    public float DropTime => dropTime;
    public int BaseScore => baseBlockPoint;
    public float MatchMult => blockMultiplier;
    public float ComboTimer => comboTimer;
}
